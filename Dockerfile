FROM bids/base_fsl

RUN apt-get update && \
apt-get install -y python3

RUN rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

COPY run.py /run.py
COPY version /version
# Make the bip directory and put it in the path
RUN mkdir /bip
ENV BIP_HOME=/bip
ENV PATH=${BIP_HOME}:${PATH}

WORKDIR ${BIP_HOME}
COPY REF REF
COPY bip.sh bip_stats.sh concat_stats1.sh optiBET.sh setup.sh bip_functions.sh \
     prep.sh concat_stats2.sh prep.sh bip_prep.sh dir_exist.py \
     fsl_anat_alt mri_deface file_counter.py bedpostx_postproc_gpu.sh ${BIP_HOME}/

ENTRYPOINT ["/run.py"]

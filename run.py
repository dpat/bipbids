#!/usr/bin/env python3
import argparse
import os
import subprocess
from glob import glob

__version__ = open(os.path.join(os.path.dirname(os.path.realpath(__file__)),
                                'version')).read()

def run(command, env={}):
    merged_env = os.environ
    merged_env.update(env)
    process = subprocess.Popen(command, stdout=subprocess.PIPE,
                               stderr=subprocess.STDOUT, shell=True,
                               env=merged_env)
    while True:
        line = process.stdout.readline()
        line = str(line, 'utf-8')[:-1]
        print(line)
        if line == '' and process.poll() != None:
            break
    if process.returncode != 0:
        raise Exception("Non zero return code: %d"%process.returncode)

parser = argparse.ArgumentParser(description='Example BIDS App entrypoint script.')
parser.add_argument('bids_dir', help='The directory with the input dataset '
                    'formatted according to the BIDS standard.')
parser.add_argument('output_dir', help='The directory where the output files '
                    'should be stored. If you are running group level analysis '
                    'this folder should be prepopulated with the results of the'
                    'participant level analysis.')
parser.add_argument('analysis_level', help='Level of the analysis that will be performed. '
                    'Multiple participant level analyses can be run independently '
                    '(in parallel) using the same output_dir.',
                    choices=['participant', 'group'])
parser.add_argument('--participant_label', help='The label(s) of the participant(s)'
                    'that should be analyzed. The label '
                   'corresponds to sub-<participant_label> from the BIDS spec '
                   '(so it does not include "sub-"). If this parameter is not '
                   'provided all subjects should be analyzed. Multiple '
                   'participants can be specified with a space separated list.',
                   nargs="+")
parser.add_argument('--session_label', help='The label of the session that should'
                    'be analyzed. The label '
                    'corresponds to ses-<session_label> from the BIDS spec '
                    '(so it does not include "ses-"). If this parameter is not '
                    'provided all sessions should be analyzed. Multiple '
                    'sessions can be specified with a space separated list.',
                    nargs="+")
# Set these stages up:
# setup (skull strip and deface which should be manually checked)
# prep (which runs bedpost...would benefit from GPUS)
# bip (which should run these processes):
#       bip_prep to set up the bip directories
#       bip to run probtrackx2 iteratively
#       bip_stats to create the descriptive stats output from bip
# stats concatenates the final statistical results
parser.add_argument('--stages', help='processing stages to run.',
                    choices=['setup', 'prep', 'bip', 'stats'])

parser.add_argument('--gpu', help='use cuda 8 for bedpostx or probtrackx2.',
                    choices=['yes', 'no'], default='no')

parser.add_argument('--tract', help='tract to run in bip stage',
                    choices=['arc_l', 'arc_r', 'aslant_l', 'aslant_r', 'b3tob3_ih', 'cst_l', 'cb2th_lr','cb2th_rl', 'extcap_l', 'extcap_r', 'fr2th_l', 'fr2th_r','ilf_l', 'ilf_r', 'iof_l', 'iof_r','mdlf_l', 'cst_r',
                    'mdlf_r', 'par2th_l', 'par2th_r','slf2_l', 'slf2_r', 'tmp2th_l', 'tmp2th_r','unc_l', 'unc_r', 'vwfa2vwfa_ih','w5tow5_ih'])

parser.add_argument('--skip_bids_validator', help='Whether or not to perform BIDS dataset validation',
                   action='store_true')
parser.add_argument('-v', '--version', action='version',
                    version='BIDS-App example version {}'.format(__version__))


args = parser.parse_args()

if not args.skip_bids_validator:
    run('bids-validator %s'%args.bids_dir)

subjects_to_analyze = []
# only for a subset of subjects
if args.participant_label:
    subjects_to_analyze = args.participant_label
# for all subjects
else:
    subject_dirs = glob(os.path.join(args.bids_dir, "sub-*"))
    subjects_to_analyze = [subject_dir.split("-")[-1] for subject_dir in subject_dirs]

# running participant level
# setup
if args.analysis_level == "participant" and args.stages == "setup":
    for subject_label in subjects_to_analyze:
        cmd = "setup.sh %s %s %s"%(subject_label,args.bids_dir,args.output_dir)
        print(cmd)
        run(cmd)

# prep
if args.analysis_level == "participant" and args.stages == "prep":
    for subject_label in subjects_to_analyze:
        cmd = "prep.sh %s %s %s %s"%(subject_label,args.bids_dir,args.output_dir,args.gpu)
        print(cmd)
        run(cmd)

# bip
if args.analysis_level == "participant" and args.stages == "bip":
    for subject_label in subjects_to_analyze:
        # bip.sh will take a tractname as an additional argument
        cmd = "bip.sh %s %s %s %s %s "%(subject_label,args.bids_dir,args.output_dir,args.tract,args.gpu)
        print(cmd)
        run(cmd)

#running group level
if args.analysis_level == "group":
    cmd = "concat_stats1.sh  %s"%(args.output_dir)
    run(cmd)
    for subject_label in subjects_to_analyze:
        cmd = "concat_stats2.sh  %s %s"%(subject_label,args.output_dir)
        print(cmd)
        run(cmd)

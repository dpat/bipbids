ACTION=setup
DOCKERFILE=Dockerfile
ENVLOC=/etc/trhenv
IMG=diannepat/bip
NAME=bip
PROG=Bip
SUBJNUM=

.PHONY: help docker exec run stop watch

help:
	@echo "Make what? Try: clean, docker, exec, run, stop, watch"
	@echo '  where:'
	@echo '     help    - show this help message'
	@echo '     clean   - remove runtime outputs: derivatives, sourcedata, error files'
	@echo '     docker  - build a ${PROG} container image'
	@echo '     exec    - exec into running ${PROG} (CLI arg: NAME=containerID)'
	@echo '     run     - start a ${PROG} container (CLI args: ACTION= and SUBJNUM=)'
	@echo '     stop    - stop a running ${PROG} container'
	@echo '     watch   - show logfile for a running ${PROG} container'

clean:
	rm -rf derivatives sourcedata
	rm -rf *error*

docker:
	docker build -t ${IMG} .

exec:
	docker cp .bash_env ${NAME}:${ENVLOC}
	docker exec -it ${NAME} bash

run:
	bip_wrap.sh ${ACTION} ${SUBJNUM}

stop:
	@docker stop ${NAME}
	@docker rm -f ${NAME}

watch:
	docker logs -f ${NAME}

#!/bin/bash
#==============================================================================

: <<COMMENTBLOCK

Function:   CheckFilesSetup
Purpose:    Determine whether the directories and files needed for processing are present.
            Create index.txt if it is missing.
            Exit gracefully, producing an error log, if there is a problem that will prevent
            setup from running.
Input:      A BIDS directory with dwi and anat files at least
Output:     Information.  If there is a problem, then ${bids_dir}/${subj}_error.txt is generated,
            and subsequent processing is prevented.
COMMENTBLOCK

CheckFilesSetup ()
{
  echo "========================================"
  echo "Checking that critical files exist for running setup."
  echo "setup will exit and produce error report if not."

  err_txt=${bids_dir}/${subj}_error.txt
  CheckDir ${bids_dir}/${subj}/ "subject dir exists" "subject dir missing"
  subj_dir_exist=${dir_exist}
  if [ "${subj_dir_exist}" = "True" ]; then

    ##################ANAT####################
    ##########################################
    echo ""
    echo "Checking anat files now"
    # Look for anat dir and related files
    CheckDir ${bids_dir}/${subj}/anat "anat dir exists" "anat dir missing"
    anat_dir_exist=${dir_exist}
    if [ "${anat_dir_exist}" = "True" ]; then
      CheckFiles ${bids_dir}/${subj}/anat/ "${subj}*T1*.nii.gz" "T1w exists" "No T1w"
      # Count the T1w images. BIP can't handle multiple ones
      T1w_count=$(CountFiles ${bids_dir}/${subj}/anat/ "${subj}*T1*.nii.gz")
      #echo "T1w_count is ${T1w_count}"
      if [ ${T1w_count} -gt 1 ]; then
        T1w_count_fail_msg="There are multiple anat/T1w volumes. This is a problem for BIP."
        touch ${err_txt}
        if [ ! -e ${err_txt} ]; then
          touch ${err_txt}
          echo "===========================================" >> ${err_txt}
          echo ${subj} >> ${err_txt}
          echo "${T1w_count_fail_msg}" >> ${err_txt}
        fi
          echo "${T1w_count_fail_msg}"
          echo "${T1w_count_fail_msg}" >> ${err_txt}
      fi
    fi

    ##################DWI####################
    ##########################################
    echo ""
    echo "Checking dwi files now"
    CheckDir ${bids_dir}/${subj}/dwi "dwi dir exists" "dwi dir missing"
    dwi_dir_exist=${dir_exist}
    if [ "${dwi_dir_exist}" = "True" ]; then
      CheckFiles ${bids_dir}/${subj}/dwi/ "${subj}_*dwi.nii.gz" "dwi image exists" "No dwi image"
      dwi_image_exist=$(FileExists "${bids_dir}/${subj}/dwi/" "${subj}_*dwi.nii.gz")
      if [ "$dwi_image_exist" = "0" ]; then
        DIM4=$(fslinfo ${bids_dir}/${subj}/dwi/${subj}_*dwi.nii.gz | grep -m 1 dim4 | awk '{print $2}')
        if [[ ${DIM4} -lt 6 ]]; then
          touch ${err_txt}
          echo "dwi has ${DIM4} volumes.  Something is wrong."; echo "dwi has ${DIM4} volumes.  Something is wrong." >> ${err_txt}
        else
          echo "The dwi has ${DIM4} volumes. Ensuring that index.txt exists."
          if [ ! -e ${bids_dir}/index*.txt ]; then
            touch ${bids_dir}/index.txt
          	numvols=$(fslinfo ${bids_dir}/${subj}/dwi/${subj}_*dwi.nii.gz | grep -m 1 dim4 | awk '{print $2}')
          	indx=" "
          	for ((i=1; i<=${numvols}; i+=1)); do indx="$indx 1"; done
          	echo $indx > ${bids_dir}/index.txt
          fi
        fi
        DIM3=$(fslinfo ${bids_dir}/${subj}/dwi/${subj}_*dwi.nii.gz | grep -m 1 dim3 | awk '{print $2}')
       # if there are fieldmaps, we presume we can run topup.
       # Check if there is not odd number of slices for topup and applytopup
       # if there is an odd number, discard the last slice of both A-P and P-A images
        # The %2 checks if DIM3 is even (divisible by 2).
        if [[ ${DIM3}%2 -eq 0 ]]; then
         echo "DWI image has even number of slices,${DIM3}. Good!"
        else
         echo "revising DWI and reverse phase encode images to have an even number of slices."
         echo "This requires write permission on these files in the bids dir."
         DIM1=$(fslinfo ${bids_dir}/${subj}/dwi/${subj}_*dwi.nii.gz | grep -m 1 dim1 | awk '{print $2}')
         DIM2=$(fslinfo ${bids_dir}/${subj}/dwi/${subj}_*dwi.nii.gz | grep -m 1 dim2 | awk '{print $2}')
         # A math expression may be placed in double ()
         ((FINsl=${DIM3} - 1))
         chmod u+w ${bids_dir}/${subj}/dwi/${subj}_*dwi.nii.gz
         fslroi ${bids_dir}/${subj}/dwi/${subj}_*dwi.nii.gz ${bids_dir}/${subj}/dwi/${subj}_*dwi.nii.gz 0 ${DIM1} 0 ${DIM2} 0 ${FINsl}
         chmod u+w ${bids_dir}/${subj}/fmap/${subj}_dir-*epi.nii.gz
         fslroi ${bids_dir}/${subj}/fmap/${subj}_dir-*epi.nii.gz ${bids_dir}/${subj}/fmap/${subj}_dir-*epi.nii.gz 0 ${DIM1} 0 ${DIM2} 0 ${FINsl}
        fi
      fi
    fi

    ##################FMAP####################
    ##########################################
    if [ -d "${bids_dir}/${subj}/fmap" ]; then
      # Look in the fmap dir if it exists
      echo ""
      echo "Checking fmap files now"
      CheckFiles ${bids_dir}/${subj}/fmap/ "${subj}_*magnitude1.nii.gz" "magnitude1 exists" "No magnitude1 image"
      CheckFiles ${bids_dir}/${subj}/fmap/ "${subj}_*magnitude2.nii.gz" "magnitude2 exists" "No magnitude2 image"
      CheckFiles ${bids_dir}/${subj}/fmap/ "${subj}_*phasediff.nii.gz" "phasediff exists" "No phasediff image"
      CheckFiles ${bids_dir}/${subj}/fmap/ "${subj}_dir-*epi.nii.gz" "ReversePE exists" "No ReversePE image"
      echo "Checking that config files exist"
      # Count the acq* config files. BIP requires 2
      acq_count=$(CountFiles ${bids_dir}/ "acq*.txt")
      if [ ${acq_count} -lt 2 ]; then
        acq_count_fail_msg="acqparams.txt and/or acq_*.txt is missing from the bids dir. This is a problem for BIP."
        touch ${err_txt}
        if [ ! -e ${err_txt} ]; then
          touch ${err_txt}
          echo "===========================================" >> ${err_txt}
          echo ${subj} >> ${err_txt}
          echo "${acq_count_fail_msg}" >> ${err_txt}
        fi
          echo "${acq_count_fail_msg}"
          echo "${acq_count_fail_msg}" >> ${err_txt}
      fi
    else
      echo "There is no fmap dir."
      echo "Processing can proceed but corrections will be less sophisticated."
    fi
  fi
}
#==============================================================================

: <<COMMENTBLOCK

Function:   Erode
Purpose:    erode a mask to make it smaller
Input:      magnitude1 image in ${bids_dir}/fmap
Output:     Eroded mask for magnitude1 image.

COMMENTBLOCK

Erode ()
{
  image=$1

  i=1
  while [ $i -le 2 ]; do
    echo "eroding ${i}"
    fslmaths ${image} -kernel 3D -ero ${image} -odt char
    let i+=1
  done
}

#==============================================================================

: <<COMMENTBLOCK
Function:   FindB0s
Purpose:    identify the locations of the B0 images from the bvals file
Caller:     PrepDWI and Top
Input:      The bvals file must exist in the subject's dwi directory
Output:     2 values representing the volume numbers of the first and
            last B0 image: B0_1 and B0_2

COMMENTBLOCK

FindB0s ()
{
  # Start the count at 0
  count=0
  # Declare an array to hold the positions of the B0 images
  declare -a B0_array=()
  # start the array index at 0
  i=0

  # Read the bval file to find the B0 volumes
  while read line; do
    for word in $line; do
      if [ "${word}" = "0" ]; then
        B0_array[$i]=${count}
        echo "B0_array[$i]=${count}"
        # increment the array index inside the if so
        # the index reflects the number of B0s rather
        # than the number of words examined
        let "i=i+1"
      fi
      # increment the count
      let "count=count+1"
    done
  done < ${bids_dir}/${subj}/dwi/${subj}*dwi.bval

  # Define the first and last elements of the array
  # using older bash syntax for arrays
  B0_1=${B0_array[@]:0:1}
  B0_2=${B0_array[${#B0_array[@]}-1]}
}

#==============================================================================

: <<COMMENTBLOCK

Function:   FSLAnatProc
Purpose:    Prep the T1w image
Input:      T1w image in ${bids_dir}/anat
Output:     T1w.anat directory

COMMENTBLOCK

FSLAnatProc ()
{
  echo "------------------------------------------------"
  started=$(date "+%Y-%m-%d %T")
  echo "Starting FSLAnatProc UTC: ${started}"

  if [ ! -d ${bids_dir}/derivatives/fsl_anat_proc/${subj}/anat/${subj}_T1w.anat ]; then
    echo "Running fsl_anat_alt with optiBET"
    if [ ! -d ${bids_dir}/derivatives/fsl_anat_proc/${subj}/anat ]; then
      mkdir -p ${bids_dir}/derivatives/fsl_anat_proc/${subj}/anat
    fi

    # Define useful variables, given that we do not know the exact name of the T1w file
    anat_img=$(ls ${bids_dir}/${subj}/anat/${subj}*T1*.nii.gz)
    anat_img_base=$(basename ${anat_img})
    anat_img_base_stem=$(remove_ext ${anat_img_base})
    chmod u+w ${bids_dir}/${subj}/anat/
    chmod u+w ${anat_img}

    echo "Copy the original T1w image to derivatives ensuring a simplified name and do the work there"
    if [ ! -e ${anat_outdir}/${subj}/anat/${subj}_T1w.nii.gz  ]; then
      cp ${anat_img} ${anat_outdir}/${subj}/anat/${subj}_T1w.nii.gz
    fi
    # Ensure orientation is correct
    fslreorient2std ${anat_outdir}/${subj}/anat/${subj}_T1w.nii.gz ${anat_outdir}/${subj}/anat/${subj}_T1w.nii.gz
    # If the lesion mask exists, then process it too.
    if [ -e ${bids_dir}/${subj}/anat/${subj}_label-lesion_roi.nii.gz ]; then
      echo "There is a lesion mask to be used in processing."
      echo "The new lesion-mask will be cropped and in:"
      echo "${anat_outdir}/${subj}/anat/${subj}_label-lesion_roi.nii.gz."
      if [ ! -e ${anat_outdir}/${subj}/anat/${subj}_label-lesion_roi.nii.gz ]; then
        cp ${bids_dir}/${subj}/anat/${subj}_label-lesion_roi.nii.gz ${anat_outdir}/${subj}/anat/
      fi
      fsl_anat_alt -i ${anat_outdir}/${subj}/anat/${subj}_T1w.nii.gz -m ${anat_outdir}/${subj}/anat/${subj}_label-lesion_roi.nii.gz --noseg --nosubcortseg

      echo "Replace original lesion mask with cropped lesion mask"
      cp ${anat_outdir}/${subj}/anat/${subj}_T1w.anat/lesionmask.nii.gz ${anat_outdir}/${subj}/anat/${subj}_label-lesion_roi.nii.gz
    else
      fsl_anat_alt -i ${anat_outdir}/${subj}/anat/${subj}_T1w.nii.gz --noseg --nosubcortseg
    fi

    echo "fsl_anat_alt has finished"
    echo "------------------------------------------------"

    echo "Replace original T1w image with cropped bias-corrected image with simple name"
    cp ${anat_outdir}/${subj}/anat/${subj}_T1w.anat/T1_biascorr.nii.gz ${anat_outdir}/${subj}/anat/${subj}_T1w.nii.gz

    echo "Copy the brain_mask to derivatives/fsl_anat_proc"
    cp ${anat_outdir}/${subj}/anat/${subj}_T1w.anat/T1_biascorr_brain_mask.nii.gz ${anat_outdir}/${subj}/anat/${subj}_T1w_mask.nii.gz
  fi

  finished=$(date "+%Y-%m-%d %T")
  echo "FSLAnatProc has completed UTC: ${finished}"
}

#==============================================================================

: <<COMMENTBLOCK

Function:   MakeACQ
Purpose:    Create acqparams.txt.
            See https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/eddy/Faq
Output:
NOT IMPLEMENTED

COMMENTBLOCK

MakeACQ ()
{
# To be used for acqp.txt and acqparams.txt:
TOTAL_READOUT_TIME=$(cat ${bids_dir}/${subj}/dwi/${subj}_*dwi.json | grep -m 1 TotalReadoutTime | sed 's/,//g' | awk '{print $2}')
}


#==============================================================================

: <<COMMENTBLOCK

Function:   MakeDirs
Purpose:    Create subdirectories we will need
Output:     directory structure

COMMENTBLOCK

MakeDirs ()
{
  mkdir -p ${output_dir}/${subj}/{dwi,reg}
  if [ -d ${bids_dir}/${subj}/fmap ]; then
     mkdir -p ${output_dir}/${subj}/fmap
  fi
}

#==============================================================================

: <<COMMENTBLOCK

Function:   PrepDWI
Purpose:    Create default B0 brain_mask by transforming T1w mask into B0 space.
            Precedes motion correction and bedpost
Input:      4D dwi image (or images, if you have reverse phase encode)
Output:     Default mask for each image. If available, fieldmaps and topup help
						create an undistorted image.
COMMENTBLOCK

PrepDWI ()
{

  echo "------------------------------------------------"
  started=$(date "+%Y-%m-%d %T")
  echo "Starting PrepDWI UTC: ${started}"

  if [ ! -e ${output_dir}/${subj}/dwi/nodif.nii.gz ]; then
    echo "------------------------------------------------"
    echo "Running DWI Brain Extraction"
    # If there is no fieldmap directory (fmap)
  	if [ ! -d ${bids_dir}/${subj}/fmap ]; then
      echo "if there is no fmap dir"
      FindB0s
  		echo "separate only one B0 from 4D image, find B0 from bval file"
      fslroi ${bids_dir}/${subj}/dwi/${subj}_*dwi.nii.gz ${output_dir}/${subj}/dwi/nodif.nii.gz $B0_1 1
  	fi
    # If there is a fieldmap directory (fmap)
  	if [ -d ${bids_dir}/${subj}/fmap ]; then
  		echo "If there is a fmap dir, topup processing, be patient"
      mkdir -p ${output_dir}/${subj}/dwi
  		Top
  	fi
  fi

	if [ ! -e ${output_dir}/${subj}/dwi/nodif_brain_mask.nii.gz ]; then
    echo "create B0 brain mask"
    # Create temporary linear registration matrix of B0 to T1w
    flirt -in ${output_dir}/${subj}/dwi/nodif -ref ${anat_outdir}/${subj}/anat/${subj}_T1w.nii.gz -omat ${output_dir}/${subj}/reg/${subj}_B02T1w_temp.mat -cost mutualinfo
    # Create temporary inverse matrix (T1w to B0)
    convert_xfm -omat ${output_dir}/${subj}/reg/${subj}_T1w2B0_temp.mat -inverse ${output_dir}/${subj}/reg/${subj}_B02T1w_temp.mat
    # Apply inverse matrix to T1w brain mask to create DWI brain mask
    flirt -in ${anat_outdir}/${subj}/anat/${subj}_T1w_mask.nii.gz -applyxfm -init ${output_dir}/${subj}/reg/${subj}_T1w2B0_temp.mat -out ${output_dir}/${subj}/dwi/nodif_brain_mask -interp nearestneighbour -ref ${output_dir}/${subj}/dwi/nodif
  else
  	echo "B0 brain mask was already created.  Moving on."
  fi

  finished=$(date "+%Y-%m-%d %T")
  echo "PrepDWI has completed UTC: ${finished}"
}

#==============================================================================

: <<COMMENTBLOCK

Function:   PrepMAG
Purpose:    Extract default mag brain_mask
Input:      Magnitude images must be available.  Pick first magnitude image if multiple ones exist.
Output:     Eroded mask for MAG image, copy of magnitude and phase images into
            derivatives directory with expected names.
            This facilitates the use of prep later.

COMMENTBLOCK

PrepMAG ()
{
  for firstmag in ${bids_dir}/${subj}/fmap/${subj}*_magnitude1.nii.gz; do
    if [ -d ${bids_dir}/${subj}/fmap ] && [ ! -e ${output_dir}/${subj}/fmap/${subj}_magnitude1_brain_mask.nii.gz ]; then
      echo "------------------------------------------------"
  	  echo "Running registration and eroding, Magnitude1 Only"
      basemag=$(basename -s _magnitude1.nii.gz ${firstmag})
      cp ${bids_dir}/${subj}/fmap/${basemag}_magnitude1.nii.gz ${output_dir}/${subj}/fmap/${subj}_magnitude1.nii.gz
      cp ${bids_dir}/${subj}/fmap/${basemag}_magnitude1.json ${output_dir}/${subj}/fmap/${subj}_magnitude1.json
      cp ${bids_dir}/${subj}/fmap/${basemag}_phasediff.nii.gz ${output_dir}/${subj}/fmap/${subj}_phasediff.nii.gz
      cp ${bids_dir}/${subj}/fmap/${basemag}_phasediff.json ${output_dir}/${subj}/fmap/${subj}_phasediff.json
      # Create temporary linear registration matrix of magnitude to T1w
      flirt -in ${output_dir}/${subj}/fmap/${subj}_magnitude1.nii.gz -ref  ${anat_outdir}/${subj}/anat/${subj}_T1w.nii.gz -omat ${output_dir}/${subj}/reg/${subj}_Mag2T1w.mat -cost mutualinfo
      # Create inverse matrix (T1w to Mag)
      convert_xfm -omat ${output_dir}/${subj}/reg/${subj}_T1w2Mag.mat -inverse ${output_dir}/${subj}/reg/${subj}_Mag2T1w.mat
      # Apply inverse matrix to T1w brain mask to create MAG brain mask
      flirt -in ${anat_outdir}/${subj}/anat/${subj}_T1w_mask.nii.gz -applyxfm -init ${output_dir}/${subj}/reg/${subj}_T1w2Mag.mat -out ${output_dir}/${subj}/fmap/${subj}_magnitude1_brain_mask -interp nearestneighbour -ref ${output_dir}/${subj}/fmap/${subj}_magnitude1.nii.gz
      Erode ${output_dir}/${subj}/fmap/${subj}_magnitude1_brain_mask
      else
        echo "The mag image was already extracted.  Moving on."
    fi
  done
}
#==============================================================================

: <<COMMENTBLOCK

Function:   Top
Purpose:    prepare dti data, topup, applytop, create B0 file.
Caller: 		setup.sh calls this through PrepDWI function
Input:      The dwi image and the reverse phase encode fieldmap must exist.
            ${bids_dir}/acqparams.txt is also necessary.
Output:     nodif (suitable for bet2), topup parameters and unwarped B0s

COMMENTBLOCK

Top ()
{
  if [ ! -e ${output_dir}/${subj}/dwi/top_out_fieldcoef.nii.gz ]; then
     echo "------------------------------------------------"
     echo "Setting up for Topup and applytopup"
     # Find the B0 images in the sequence by using the bvals file.
     # Average the first and last B0 to create blip1.
     FindB0s
     # Default Siemens stretchy eyeballs, blip down -1 A-P
     # Get ${B0_1} and concatenate it with ${B0_2} to create blip1
     fslroi ${bids_dir}/${subj}/dwi/${subj}_*dwi.nii.gz ${output_dir}/${subj}/dwi/blip1_a ${B0_1} 1
     fslroi ${bids_dir}/${subj}/dwi/${subj}_*dwi.nii.gz ${output_dir}/${subj}/dwi/blip1_b ${B0_2} 1
     # Create blip1 by merging blip1_a and blip1_b
     fslmerge -t ${output_dir}/${subj}/dwi/blip1 ${output_dir}/${subj}/dwi/blip1_a ${output_dir}/${subj}/dwi/blip1_b
     # Remove blip_1a and blip_1b
     imrm ${output_dir}/${subj}/dwi/blip1_a
     imrm ${output_dir}/${subj}/dwi/blip1_b

     # Reverse phase encode +1, blip_up, squishy eyeballs, P-A
  	 # Here we assume the blip2 image consists of at least 2 B0 images
     fslroi ${bids_dir}/${subj}/fmap/${subj}_dir-*epi.nii.gz ${output_dir}/${subj}/fmap/blip2 0 2
     # acqparameters are -1 then +1, so blip_down, blip_up or A-P, P-A
     fslmerge -t ${output_dir}/${subj}/dwi/b0_combo ${output_dir}/${subj}/dwi/blip1 ${output_dir}/${subj}/fmap/blip2
     echo "Starting topup"
  	 # This generates the topup parameters for the unwarping
     topup --imain=${output_dir}/${subj}/dwi/b0_combo --datain=${bids_dir}/acqparams.txt --config=b02b0.cnf --out=${output_dir}/${subj}/dwi/top_out --fout=${output_dir}/${subj}/dwi/top_hz --verbose
  else
  	 echo "topup has already been run. Moving on."
  fi

  # Run applytopup simply to get some unwarped B0 images to skull strip
  # and feed to eddy later. The result is nodif.nii.gz
  if [ ! -e ${output_dir}/${subj}/dwi/b0_hifi.nii.gz ]; then
     echo "Starting applytopup on B0 images, new version"
     applytopup --imain=${output_dir}/${subj}/dwi/blip1,${output_dir}/${subj}/fmap/blip2 --inindex=1,3 --method=jac --datain=${bids_dir}/acqparams.txt --topup=${output_dir}/${subj}/dwi/top_out --out=${output_dir}/${subj}/dwi/b0_hifi --verbose
     echo "Running fslroi to separate one B0 from applytopup result"
     fslroi ${output_dir}/${subj}/dwi/b0_hifi ${output_dir}/${subj}/dwi/nodif 0 1
  else
  	echo "applytopup work is already done. Moving on."
  fi
}


#==============================================================================

: <<COMMENTBLOCK

Function:   UserMessage
Purpose:    Inform user of needed manual checks.
            Provide file that can be run to perform the checks

COMMENTBLOCK

UserMessage ()
{
  mskchk=${bids_dir}/mask_check.txt
  touch ${mskchk}
  chmod a+x ${mskchk}

  subjexists=$(cat ${mskchk} | grep -q ${subj}; echo $?)
  # grep returns 1 if the string does not exist and 0 if it does exist
  # So, if the test=1, then add subject to mask_check.txt
  if [ ${subjexists} -eq 1 ]; then
    echo "echo ${subj}">> ${mskchk}
    max=$(fslstats  ${anat_outdir}/${subj}/anat/${subj}_T1w.nii.gz -r | awk '{print $2}')
    echo "fsleyes  derivatives/fsl_anat_proc/${subj}/anat/${subj}_T1w.nii.gz -dr 0 ${max} derivatives/fsl_anat_proc/${subj}/anat/${subj}_T1w_mask.nii.gz -cm Red -a 40" >> ${mskchk}
    echo "fsleyes derivatives/fsl_dwi_proc/${subj}/dwi/nodif.nii.gz derivatives/fsl_dwi_proc/${subj}/dwi/nodif_brain_mask.nii.gz -cm Red -a 40" >> ${mskchk}
    if [ -d ${bids_dir}/${subj}/fmap ]; then
      echo "fsleyes ${subj}/fmap/${subj}_*magnitude1.nii.gz derivatives/fsl_dwi_proc/${subj}/fmap/${subj}_magnitude1_brain_mask.nii.gz -cm Red -a 40" >> ${mskchk}
    fi
    echo "#############" >> ${mskchk}
  fi

  echo "=================================================================="
  echo "*************************  USER MESSAGE  *************************"
  echo "Please check your masks:"
  echo "From the bids directory, run masks_check.txt to view files for each subject:"
  echo ">./mask_check.txt"
  echo "Edit the binary masks if needed."
  echo "Your mask edits, if any, will take effect when you run the prep step."
  echo "------------------------------------------------------------------"
  echo "Ensure the T1w mask covers the brain, the whole brain, and nothing but the brain."
  echo "------------------------------------------------------------------"
  echo "Ensure the nodif_brain_mask covers the B0 brain, the whole brain, "
  echo "and nothing but the brain. This frequently needs manual intervention."
  echo "------------------------------------------------------------------"
  if [ -d ${bids_dir}/${subj}/fmap ]; then
    echo "Check the magnitude1 mask. This must never include non-brain."
    echo "It is fine, however, if it misses some of the brain."
  fi
  echo "=================================================================="
}

#==============================================================================

: <<COMMENTBLOCK
Function:   UpdateBidsIgnore
Purpose:    If .bidsignore is not set to ignore *.txt files, then set it to
Caller:
Input:
Output:     A revised .bidsignore

COMMENTBLOCK

UpdateBidsIgnore ()
{
  # Tell bids validator to ignore txt files
  touch ${bids_dir}/.bidsignore
  # Use grep -q to test for the existence of the string txt in .bidsignore
  stringexists=$(cat ${bids_dir}/.bidsignore | grep -q txt; echo $?)
  # grep returns 1 if the string does not exist and 0 if it does exist
  #So, if the test=1, then add *.txt to .bidsignore
  if [ ${stringexists} -eq 1 ]; then
    # Add *.txt to the items in .bidsignore
    echo "*.txt" >> ${bids_dir}/.bidsignore
    echo ".bidignore now contains the following:"
    cat ${bids_dir}/.bidsignore
  fi
}


#==============================================================================

: <<COMMENTBLOCK

Function:   HelpMessage
Purpose:    Display help
Calls:      None

COMMENTBLOCK

HelpMessage ()
{
  echo
  echo "Assuming a bids compliant data directory,"
  echo "run setup.sh with one argument, the subjectnum:"
  echo "e.g., If your subject is sub-327: setup.sh 327"
  echo "defacing, and masking will be generated for you to check."
  echo "you should be in your bids_dir"
  echo
  exit 1
}

#==============================================================================

: <<COMMENTBLOCK

Function:   Main
Purpose:    Preprocess and generate masks and segmentations
Input:      A compliant bids directory with dwi and structural data
            Optional fieldmaps and reverse phase encode data can be used
            if available.
Output:     Defaced T1w data, T1w and B0 (nodif) mask, optional magnitude mask.
            All masks should be examined and edited as needed, especially the
            nodif mask. T1w to MNI warps, T1w segmentations.
COMMENTBLOCK

Main ()
{
  CheckFilesSetup
  if [ -e ${bids_dir}/${subj}_error.txt ]; then
    echo "Critical files are missing or incorrect!"
    echo "setup cannot be run"
    echo "See ${subj}_error.txt"
    echo "If you have corrected the errors, then remove ${subj}_error.txt and try running again."
  elif [ ! -e ${bids_dir}/${subj}_error.txt ]; then
    started_setup=$(date "+%Y-%m-%d %T")
    echo "Congratulations! No error file generated, running setup now."
    echo "Starting setup UTC: ${started_setup}"
    echo ""
    echo "setup will create output directories under derivatives:"
    echo "fsl_dwi_proc and fsl_anat_proc."
    echo "fsl_dwi_proc: will contain processed dwi images, registrations,"
    echo "and if you have appropriate fieldmap files, fmap images."
    echo "fsl_anat_proc: will contain cropped and segmented T1w data."
    echo "=================================================================="
    UpdateBidsIgnore
    MakeDirs
    FSLAnatProc
    PrepDWI
    if [ -d ${bids_dir}/${subj}/fmap ]; then
      echo "Start PrepMAG to mask the magnitude fieldmap image"
      PrepMAG
    fi
    UserMessage
    finished_setup=$(date "+%Y-%m-%d %T")
    echo "Finished setup UTC: ${finished}"
  fi
}

#==============================================================================
: <<COMMENTBLOCK
See Main
COMMENTBLOCK

source ${BIP_HOME}/bip_functions.sh
# We pass 3 arguments from run.py: the subjectnum, bids_dir and output_dir

subjectnum=$1
subj=sub-${subjectnum}
bids_dir=$2
output_dir=$3
bip_outdir=${bids_dir}/derivatives/bip
anat_outdir=${bids_dir}/derivatives/fsl_anat_proc
dwi_outdir=${bids_dir}/derivatives/fsl_dwi_proc
###################
echo "subject=${subj}"

if [ $# -lt 1 ]
  then
    HelpMessage
    exit 1
fi

Main ${subjectnum} ${bids_dir} ${output_dir}
#==============================================================================

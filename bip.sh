#!/bin/bash
#==============================================================================

: <<COMMENTBLOCK

###############################################################################
AUTHOR:         Dianne Patterson University of Arizona
DATE CREATED:   April 16, 2009, revised 3/25/18.
===============================================================================
----------------------------  PURPOSE  ----------------------------------------
General purpose probability tracking for constrained tracts using an iterative
parcellation technique. What I do is to track back and forth between the 2
seeds creating a classification mask each time.The classification masks thus
created get smaller and smaller, thus converging on the optimized mask. This
calls bip_sub to do a lot of the work.
Major mods 2/2011
===============================================================================
----------------------------  INPUT  ------------------------------------------
One tract name, e.g., arc_l.
It is assumed that the correct infrastructure of lists, images and directories
already exists, because bip_prep.sh must have already been run.

===============================================================================
----------------------------  OUTPUT  -----------------------------------------
A set of iterations (these can take a couple of days depending on the tract),
and a log of endpoint values at each iteration.  The final rois should be the
bip output.

###############################################################################

COMMENTBLOCK
###############################################################################
#########################  DEFINE FUNCTIONS  ##################################

#==============================================================================

: <<COMMENTBLOCK

Function:   CheckFilesBip
Purpose:    Determine whether files needed for processing are present.
            Exit gracefully with errors if there is a problem.
Input:      A BIDS directory with dwi and anat files.

Output:     Information.  If there is a problem, then an error file is generated.

COMMENTBLOCK

CheckFilesBip ()
{
  echo "========================================"
  echo "Checking that critical files exist for running bip."
  echo "bip will exit and produce error report if not."

err_txt=${bids_dir}/${subj}_error.txt

  CheckDir ${bids_dir}/${subj}/ "subject dir exists" "subject dir missing"
  subj_dir_exist=${dir_exist}
  if [ "${subj_dir_exist}" = "True" ]; then
    subj_dwi_out=${dwi_outdir}/${subj}/dwi/
    CheckDir ${subj_dwi_out} "subject fsl_dwi_proc dir exists" "subject fsl_dwi_proc dir missing"
    subj_dwi_dir_exist=${dir_exist}
    if [ "${subj_dwi_dir_exist}" = "True" ]; then
      echo "subj_dwi_out is ${subj_dwi_out}"
      CheckFiles ${subj_dwi_out} "nodif_brain.nii.gz" "nodif_brain image exists" "No ${subj_dwi_out}/nodif_brain.nii.gz"
      echo ""
      ####################EDDY###################
      ###########################################
      if [ -d "${bids_dir}/${subj}/fmap" ]; then
        # Look in the fmap dir if it exists
      echo "Checking for Eddy output"
      CheckFiles ${subj_dwi_out} "data.eddy_movement_rms" "data.eddy_movement_rms exists" "No ${subj_dwi_out}/data.eddy_movement_rms"
      CheckFiles ${subj_dwi_out} "data.eddy_outlier_map" "data.eddy_outlier_map exists" "No ${subj_dwi_out}/data.eddy_outlier_map"
      CheckFiles ${subj_dwi_out} "data.eddy_outlier_n_stdev_map" "data.eddy_outlier_n_stdev_map exists" "No ${subj_dwi_out}/data.eddy_outlier_n_stdev_map"
      CheckFiles ${subj_dwi_out} "data.eddy_outlier_report" "data.eddy_outlier_report exists" "No ${subj_dwi_out}/data.eddy_outlier_report"
      CheckFiles ${subj_dwi_out} "data.eddy_parameters" "data.eddy_parameters exists" "No ${subj_dwi_out}/data.eddy_parameters"
      CheckFiles ${subj_dwi_out} "data.eddy_post_eddy_shell_alignment_parameters" "data.eddy_post_eddy_shell_alignment_parameters exists" "No ${subj_dwi_out}/data.eddy_post_eddy_shell_alignment_parameters"
      CheckFiles ${subj_dwi_out} "data.eddy_rotated_bvecs" "data.eddy_rotated_bvecs exists" "No ${subj_dwi_out}/data.eddy_rotated_bvecs"
     fi
      # These dwi files must always exist
      CheckFiles ${subj_dwi_out} "data.nii.gz" "data.nii.gz exists" "No ${subj_dwi_out}/data.nii.gz "
      ####################DTIFIT#################
      ###########################################
      echo ""
      echo "Checking for DTIFIT output"
      CheckFiles ${subj_dwi_out} "${subj}_FA.nii.gz" "FA image exists" "No ${subj_dwi_out}/${subj}_FA.nii.gz"
      CheckFiles ${subj_dwi_out} "${subj}_L1.nii.gz" "L1 image exists" "No ${subj_dwi_out}/${subj}_L1.nii.gz"
      CheckFiles ${subj_dwi_out} "${subj}_L2.nii.gz" "L2 image exists" "No ${subj_dwi_out}/${subj}_L2.nii.gz"
      CheckFiles ${subj_dwi_out} "${subj}_L3.nii.gz" "L3 image exists" "No ${subj_dwi_out}/${subj}_L3.nii.gz"
      CheckFiles ${subj_dwi_out} "${subj}_MD.nii.gz" "MD image exists" "No ${subj_dwi_out}/${subj}_MD.nii.gz"
      CheckFiles ${subj_dwi_out} "${subj}_MO.nii.gz" "MO image exists" "No ${subj_dwi_out}/${subj}_MO.nii.gz"
      CheckFiles ${subj_dwi_out} "${subj}_RD.nii.gz" "RD image exists" "No ${subj_dwi_out}/${subj}_RD.nii.gz"
      CheckFiles ${subj_dwi_out} "${subj}_S0.nii.gz" "SO image exists" "No ${subj_dwi_out}/${subj}_S0.nii.gz"
      CheckFiles ${subj_dwi_out} "${subj}_sse.nii.gz" "sse image exists" "No ${subj_dwi_out}/${subj}_sse.nii.gz"
      CheckFiles ${subj_dwi_out} "${subj}_V1.nii.gz" "V1 image exists" "No ${subj_dwi_out}/${subj}_V1.nii.gz"
      CheckFiles ${subj_dwi_out} "${subj}_V2.nii.gz" "V2 image exists" "No ${subj_dwi_out}/${subj}_V2.nii.gz"
      CheckFiles ${subj_dwi_out} "${subj}_V3.nii.gz" "V3 image exists" "No ${subj_dwi_out}/${subj}_V3.nii.gz"
      ###################BEDPOST#################
      ###########################################
      subj_bedpost=${dwi_outdir}/${subj}/dwi.bedpostX
      echo ""
      echo "Checking for BEDPOST output: ${subj_bedpost}"
      CheckDir ${subj_bedpost} "subject bedpost dir exists" "subject bedpost dir missing"
      subj_bedpost_exist=${dir_exist}
      if [ "${subj_bedpost_exist}" = "True" ]; then
        CheckFiles ${subj_bedpost} "dyads1_dispersion.nii.gz" "dyads1_dispersion image exists" "No ${subj_bedpost}/dyads1_dispersion.nii.gz"
        CheckFiles ${subj_bedpost} "dyads1.nii.gz" "dyads1 image exists" "No ${subj_bedpost}/dyads1.nii.gz"
        CheckFiles ${subj_bedpost} "dyads2_dispersion.nii.gz" "dyads2_dispersion image exists" "No ${subj_bedpost}/dyads2_dispersion.nii.gz"
        CheckFiles ${subj_bedpost} "dyads2_thr0.05_modf2.nii.gz" "dyads2_thr0.05_modf2 image exists" "No ${subj_bedpost}/dyads2_thr0.05_modf2.nii.gz"
        CheckFiles ${subj_bedpost} "dyads2_thr0.05.nii.gz" "dyads2_thr0.05 image exists" "No ${subj_bedpost}/dyads2_thr0.05.nii.gz"
        CheckFiles ${subj_bedpost} "dyads2.nii.gz" "dyads2 image exists" "No ${subj_bedpost}/dyads2.nii.gz"
        CheckFiles ${subj_bedpost} "dyads3_dispersion.nii.gz" "dyads3_dispersion image exists" "No ${subj_bedpost}/dyads3_dispersion.nii.gz"
        CheckFiles ${subj_bedpost} "dyads3_thr0.05_modf3.nii.gz" "dyads3_thr0.05_modf3 image exists" "No ${subj_bedpost}/dyads3_thr0.05_modf3.nii.gz"
        CheckFiles ${subj_bedpost} "dyads3_thr0.05.nii.gz" "dyads3_thr0.05 image exists" "No ${subj_bedpost}/dyads3_thr0.05.nii.gz"
        CheckFiles ${subj_bedpost} "dyads3.nii.gz" "dyads3 image exists" "No ${subj_bedpost}/dyads3.nii.gz"
        CheckFiles ${subj_bedpost} "mean_d_stdsamples.nii.gz" "mean_d_stdsamples image exists" "No ${subj_bedpost}/mean_d_stdsamples.nii.gz"
        CheckFiles ${subj_bedpost} "mean_dsamples.nii.gz" "mean_dsamples image exists" "No ${subj_bedpost}/mean_dsamples.nii.gz"
        CheckFiles ${subj_bedpost} "mean_f1samples.nii.gz" "mean_f1samples image exists" "No ${subj_bedpost}/mean_f1samples.nii.gz"
        CheckFiles ${subj_bedpost} "mean_f2samples.nii.gz" "mean_f2samples image exists" "No ${subj_bedpost}/mean_f2samples.nii.gz"
        CheckFiles ${subj_bedpost} "mean_f3samples.nii.gz" "mean_f3samples image exists" "No ${subj_bedpost}/mean_f3samples.nii.gz"
        CheckFiles ${subj_bedpost} "mean_fsumsamples.nii.gz" "mean_fsumsamples image exists" "No ${subj_bedpost}/mean_fsumsamples.nii.gz"
        CheckFiles ${subj_bedpost} "mean_ph1samples.nii.gz" "mean_ph1samples image exists" "No ${subj_bedpost}/mean_ph1samples.nii.gz"
        CheckFiles ${subj_bedpost} "mean_ph2samples.nii.gz" "mean_ph2samples image exists" "No ${subj_bedpost}/mean_ph2samples.nii.gz"
        CheckFiles ${subj_bedpost} "mean_ph3samples.nii.gz" "mean_ph3samples image exists" "No ${subj_bedpost}/mean_ph3samples.nii.gz"
        CheckFiles ${subj_bedpost} "mean_S0samples.nii.gz" "mean_S0samples image exists" "No ${subj_bedpost}/mean_S0samples.nii.gz"
        CheckFiles ${subj_bedpost} "mean_th1samples.nii.gz" "mean_th1samples image exists" "No ${subj_bedpost}/mean_th1samples.nii.gz"
        CheckFiles ${subj_bedpost} "mean_th2samples.nii.gz" "mean_th2samples image exists" "No ${subj_bedpost}/mean_th2samples.nii.gz"
        CheckFiles ${subj_bedpost} "mean_th3samples.nii.gz" "mean_th3samples image exists" "No ${subj_bedpost}/mean_th3samples.nii.gz"
        CheckFiles ${subj_bedpost} "merged_f1samples.nii.gz" "merged_f1samples image exists" "No ${subj_bedpost}/merged_f1samples.nii.gz"
        CheckFiles ${subj_bedpost} "merged_f2samples.nii.gz" "merged_f2samples image exists" "No ${subj_bedpost}/merged_f2samples.nii.gz"
        CheckFiles ${subj_bedpost} "merged_f3samples.nii.gz" "merged_f3samples image exists" "No ${subj_bedpost}/merged_f3samples.nii.gz"
        CheckFiles ${subj_bedpost} "merged_ph1samples.nii.gz" "merged_ph1samples image exists" "No ${subj_bedpost}/merged_ph1samples.nii.gz"
        CheckFiles ${subj_bedpost} "merged_ph2samples.nii.gz" "merged_ph2samples image exists" "No ${subj_bedpost}/merged_ph2samples.nii.gz"
        CheckFiles ${subj_bedpost} "merged_ph3samples.nii.gz" "merged_ph3samples image exists" "No ${subj_bedpost}/merged_ph3samples.nii.gz"
        CheckFiles ${subj_bedpost} "merged_th1samples.nii.gz" "merged_th1samples image exists" "No ${subj_bedpost}/merged_th1samples.nii.gz"
        CheckFiles ${subj_bedpost} "merged_th2samples.nii.gz" "merged_th2samples image exists" "No ${subj_bedpost}/merged_th2samples.nii.gz"
        CheckFiles ${subj_bedpost} "merged_th3samples.nii.gz" "merged_th3samples image exists" "No ${subj_bedpost}/merged_th3samples.nii.gz"
      fi
    fi
  fi
}

#==============================================================================

: <<COMMENTBLOCK

Function:   CleanIteration
Purpose:    Cleanup after probtrackx2
Input:      Results of PBX2
Output:     An iteration directory and some renamed and binarized output
            The directory, rois and fdt_paths are all numbered with the result
            number (rnum; so the first directory and fdt_paths will be 3.)

COMMENTBLOCK

CleanIteration ()
{
  # Clean up and rename files before the next iteration
  if [[ ! -d ${tract_dir}/files_${rnum} ]]; then
    mkdir ${tract_dir}/files_${rnum}
  fi
  # move targets.txt and waytotal into the files subdir
  mv ${tract_dir}/targets.txt ${tract_dir}/files_${rnum}
  mv ${tract_dir}/waytotal ${tract_dir}/files_${rnum}
  mv ${tract_dir}/probtrackx.log ${tract_dir}/files_${rnum}
  # number fdt_paths and leave it in the tract_dir
  immv ${tract_dir}/fdt_paths ${tract_dir}/fdt_paths_${rnum}
  # make a binary mask of the result roi (seeds-to-target)
  fslmaths ${tract_dir}/seeds* -bin ${tract_dir}/roi_${rnum} -odt char
  # move the non-binarized roi (seeds-to-target) into the files subdir
  mv ${tract_dir}/seeds* ${tract_dir}/files_${rnum}
  # Get the volumes for comparison and the log
  GetVols
  # echo the result roi into biplog
  printf "%s\t%s\t%s\t%s\n" ${subj} ${tract} ${rnum} ${vol_result} >> ${tract_dir}/biplog_${tract}_${subj}.tsv
}

#==============================================================================

: <<COMMENTBLOCK

Function:   CleanIterationFinal
Purpose:    Cleanup after probtrackx2 if this is the final iteration
Input:      Results of PBX2
Output:     *final* rois and tract

COMMENTBLOCK

CleanIterationFinal ()
{
  # Copy the final endpoints if they don't exist
  if [[ ! -e ${tract_dir}/roi_A_${tract}_${subj}_final.nii.gz && ! -e ${tract_dir}/roi_B_${tract}_${subj}_final.nii.gz ]]; then
    if [[ ${rnum}%2 -eq 0 ]]; then
      imcp ${tract_dir}/roi_${rnum} ${tract_dir}/roi_B_${tract}_${subj}_final
      imcp ${tract_dir}/roi_${tnum} ${tract_dir}/roi_A_${tract}_${subj}_final
      echo "result is even: ${tract_dir}/roi_${rnum} --> ${tract_dir}/roi_B_${tract}_${subj}_final and "
      echo "${tract_dir}/roi_${tnum} --> ${tract_dir}/roi_A_${tract}_${subj}_final"
    elif [[ ${rnum}%2 -ne 0 ]]; then
      imcp ${tract_dir}/roi_${rnum} ${tract_dir}/roi_A_${tract}_${subj}_final
      imcp  ${tract_dir}/roi_${tnum} ${tract_dir}/roi_B_${tract}_${subj}_final
      echo "result is odd: ${tract_dir}/roi_${rnum} --> ${tract_dir}/roi_A_${tract}_${subj}_final and "
      echo "${tract_dir}/roi_${tnum} --> ${tract_dir}/roi_B_${tract}_${subj}_final"
    fi
    # Truncate the endpoint masks so they are entirely in the grey matter.
    fslmaths ${tract_dir}/roi_A_${tract}_${subj}_final -mul ${output_dir}/${subj}/subjectrois/${subj}_B0_gmseg_bin ${tract_dir}/roi_A_${tract}_${subj}_final -odt char

    fslmaths ${tract_dir}/roi_B_${tract}_${subj}_final -mul ${output_dir}/${subj}/subjectrois/${subj}_B0_gmseg_bin ${tract_dir}/roi_B_${tract}_${subj}_final -odt char
  fi

  # This is the final tract
  # Average the 2 tracts together and create fdt.nii.gz
  if [[ ! -e ${tract_dir}/tract_${tract}_${subj}_final.nii.gz && -e ${tract_dir}/fdt_paths_${tnum}.nii.gz ]]; then
    fslmaths ${tract_dir}/fdt_paths_${tnum} -add ${tract_dir}/fdt_paths_${rnum} -div 2 ${tract_dir}/fdt
    # Now threshold the average tract image fdt at 5p
    fslmaths ${tract_dir}/fdt -thrp 5 ${tract_dir}/fdt_5p
    # # Create a binary version of the thresholded tract image
    fslmaths ${tract_dir}/fdt_5p -bin ${tract_dir}/fdt_bin -odt char
    # Subtract the endpoints from the binary tract mask and multiply by the
    # WM segmentation to create the final
    # truncated tract mask: ${tract_dir}/tract_${tract}_${subj}_bin_final
    fslmaths ${tract_dir}/fdt_bin -sub ${tract_dir}/roi_A_${tract}_${subj}_final -sub ${tract_dir}/roi_B_${tract}_${subj}_final -mul  ${output_dir}/${subj}/subjectrois/${subj}_B0_wmseg_bin -bin ${tract_dir}/tract_${tract}_${subj}_bin_final -odt char
    # Within the truncated tract mask, reinstate the probabalistic
    # information, to create the final tract image
    # ${tract_dir}/${tract}_final
    fslmaths ${tract_dir}/fdt_5p -mul ${tract_dir}/tract_${tract}_${subj}_bin_final ${tract_dir}/tract_${tract}_${subj}_final
  fi
}

#==============================================================================

: <<COMMENTBLOCK

Function:   GetVols
Purpose:    Get volumes as number of voxels for comparisons and logs
Input:      Masks
Output:     Variables vol_seed and vol_result

COMMENTBLOCK

GetVols ()
{
  # calculate the volumes of roi_${snum} and roi_${rnum}}
  vol_seed=$(fslstats ${tract_dir}/roi_${snum} -V | cut -d " " -f 1)
  vol_result=$(fslstats ${tract_dir}/roi_${rnum} -V | cut -d " " -f 1)
}


#==============================================================================
: <<COMMENTBLOCK

Function:   Iterate
Purpose:    Run probtrackx2 iteratively until endpoint size stabilizes
Input:      Directory for tract
Output:     Finished dir and images

If the seed has stabilized (or the result is volume 0),
mark rois and tracts as final and finish iterating.
Odd numbered iterations are always the "A" iteration (first roi in masks.txt)
Even numbered iterations are always the "B" iterations (second roi in masks.txt).

The last 2 tracts (from the iterations) will be averaged together.

if bip crashes, restart if the *final* images do not exist.

COMMENTBLOCK

Iterate ()
{
  # While the *final* files do not exist, iterate
  if [[ -e ${tract_dir}/roi_1.nii.gz && -e ${tract_dir}/roi_2.nii.gz ]]; then
    while [[ ! -e ${tract_dir}/tract_${tract}_${subj}_final.nii.gz ]]; do
      #   # Run probtrackx2 if the result (rnum) does not exist, and cleanup
      if [ ! -e ${tract_dir}/roi_${rnum}.nii.gz ] && [ -e ${tract_dir}/roi_${snum}.nii.gz ] && [ -e ${tract_dir}/roi_${tnum}.nii.gz ]; then
        # Run probtrackx2
        PBX2 ${tract_dir}/roi_${snum} ${tract_dir}/roi_${tnum}
        CleanIteration
      elif [ -e ${tract_dir}/roi_${rnum}.nii.gz ]; then
        GetVols
        if [[ ${vol_seed} != ${vol_result} && ${vol_result} != "0" ]]; then
          let snum+=1
          let tnum+=1
          let rnum+=1
          echo "seed_number=$snum; target_number=$tnum; result_number=$rnum"
        elif [[ ${vol_seed} = ${vol_result} || ${vol_result} = "0" ]]; then
          CleanIterationFinal
        fi
      fi
    done
  elif [[ ! -e ${tract_dir}/roi_1.nii.gz || ! -e ${tract_dir}/roi_2.nii.gz ]]; then
    echo "####################################"
    echo "#########  ERRROR!!  ###############"
    echo "I quit, roi_1 or roi_2 is missing!"
    echo "${subj} and ${tract}"
    echo "####################################"
    echo "####################################"
    exit 1
  fi
}

#==============================================================================
: <<COMMENTBLOCK

Function:   PBX2
Purpose:    Do the heavy lifting of running probtrackx2
Input:      <tract> <seed> <target> <output suffix for new seed roi>
            E.g.: $0 arc_l arc_l_bip/roi_A_1 arc_l_bip/roi_B_2 A_3
Output:     an iteration

COMMENTBLOCK

PBX2 ()
{
  seed=$1
  target=$2
  echo "seed is ${seed}"
  echo "target is ${target}"
  echo "${target}" > ${tract_dir}/targets.txt

  if [ "${gpu}" = "no" ]; then
     touch ${tract_dir}/GPU_BIP_NO
     probtrackx2 -x ${seed} -l -c 0.2 -S 2000 \
     --steplength=0.5 -P 5000 \
     --stop=${output_dir}/${subj}/subjectrois/${tract}_inv_mask_diff_bin_csf --forcedir --opd \
     -s ${subj_bedpost}/merged -m ${subj_bedpost}/nodif_brain_mask --dir=${tract_dir} \
     --targetmasks=${tract_dir}/targets.txt --os2t #-V 1
  elif [ "${gpu}" = "yes" ]; then
    touch ${tract_dir}/GPU_BIP_YES
    probtrackx2_gpu -x ${seed} -l -c 0.2 -S 2000 \
    --steplength=0.5 -P 5000 \
    --stop=${output_dir}/${subj}/subjectrois/${tract}_inv_mask_diff_bin_csf --forcedir --opd \
    -s ${subj_bedpost}/merged -m ${subj_bedpost}/nodif_brain_mask --dir=${tract_dir} \
    --targetmasks=${tract_dir}/targets.txt --os2t #-V 1
  fi
}

#==============================================================================
: <<COMMENTBLOCK

Function:   HelpMessage
Purpose:    Display help if user enters fewer than one argument
Input:      None
Output:     Help Message

COMMENTBLOCK

HelpMessage ()
{
  echo "Usage: $0 <tract> <subjectnum>"
  echo "Example: $0 cst_l 327"
  echo "Runs bip.sh on the cst_l tract for sub-327"
  exit 1
}

#==============================================================================
: <<COMMENTBLOCK

Function:   Main
Purpose:    Run bip_prep, bidirectional iterative parcellation and bip_stats
Input:      tract subjectnum e.g., arc5_l 327
            This assumes a supported tract from the list, inverse mask and
            starter rois
Output:     tract subdir under dwi, Iterations for the tract, stats

COMMENTBLOCK

Main ()
{
  CheckFilesBip
  if [ -e "${bids_dir}/${subj}_error.txt" ]; then
    echo "Critical files are missing or incorrect!"
    echo "bip cannot be run"
    echo "See ${subj}_error.txt"
    echo "If you have corrected the errors, then remove ${subj}_error.txt and try running again."
  elif [ ! -e "${bids_dir}/${subj}_error.txt" ]; then
    echo "Congratulations!  No error file generated!"
    echo "start bip_prep"
    if [ ! -d ${output_dir}/${subj}/dwi ]; then
      mkdir -p ${output_dir}/${subj}/dwi
    fi
    bip_prep.sh ${subjectnum} ${bids_dir} ${output_dir} ${tract}
    echo "end bip_prep"
    # define counters
    snum=1 # for seed number
    let tnum=${snum}+1 # for target number
    let rnum=${tnum}+1 # for result number

    if [ ! -e ${tract_dir}/biplog_${tract}_${subj}.tsv ]; then
      # Get the first 2 rois and rename them with the scheme
      roi_1=$(head -n 1 ${tract_dir}/masks.txt)
      roi_2=$(tail -n 1 ${tract_dir}/masks.txt)
      imcp ${output_dir}/${subj}/subjectrois/${roi_1} ${tract_dir}/roi_1
      imcp ${output_dir}/${subj}/subjectrois/${roi_2} ${tract_dir}/roi_2
      echo "${tract_dir}/roi_2.nii.gz" > ${tract_dir}/targets.txt
      # create bip_log and header and first 2 entries
      printf "%s\t%s\t%s\t%s\n" "subject" "tract" "roi" "voxcount" > ${tract_dir}/biplog_${tract}_${subj}.tsv
      vol_1=$(fslstats ${tract_dir}/roi_1 -V | cut -d " " -f 1)
      vol_2=$(fslstats ${tract_dir}/roi_2 -V | cut -d " " -f 1)

      # print the first two roi stats into biplog
      printf "%s\t%s\t%s\t%s\n" ${subj} ${tract} 1 ${vol_1} >> ${tract_dir}/biplog_${tract}_${subj}.tsv
      printf "%s\t%s\t%s\t%s\n" ${subj} ${tract} 2 ${vol_2} >> ${tract_dir}/biplog_${tract}_${subj}.tsv
    fi
    echo "start bip"
    # Do the work
    Iterate
    CleanIterationFinal
    echo "end bip"
    echo "start bip_stats"
    bip_stats.sh ${subjectnum} ${bids_dir} ${output_dir} ${tract}
    echo "end bip_stats"
  fi
}

#########################  END FUNCTION DEFINITIONS  ##########################
###############################################################################

source ${BIP_HOME}/bip_functions.sh
if [ $# -lt 1 ]
then
  HelpMessage
  exit 1
fi
#
# # We pass 5 arguments from run.py: the subjectnum, bids_dir and output_dir, tract
subjectnum=$1
subj=sub-${subjectnum}
bids_dir=$2
output_dir=$3
tract=$4
gpu=$5
bip_outdir=${bids_dir}/derivatives/bip #should be the same as output_dir at this point
anat_outdir=${bids_dir}/derivatives/fsl_anat_proc
dwi_outdir=${bids_dir}/derivatives/fsl_dwi_proc
subj_bedpost=${dwi_outdir}/${subj}/dwi.bedpostX
#
tract_dir=${output_dir}/${subj}/dwi/${tract}_bip
inv=${output_dir}/${subj}/subjectrois/${tract}_inv_mask_diff_bin_csf

echo "subject=${subj}"
echo "bids_dir = ${bids_dir}"
echo "output_dir = ${output_dir}"
echo "tract= ${tract}"
echo "tract_dir=${output_dir}/${subj}/dwi/${tract}_bip"
echo "gpu=${gpu}"

Main ${subjectnum} ${bids_dir} ${output_dir} ${tract}
